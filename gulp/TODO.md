# TO DO

* ~Refac da task Script~
* Refac da task de Deploy
* Opção para comprimir saída da task Styles
* Padronizar chaves 'optimize', 'uglify' e 'output'
* Documentação ao longo do arquivo config.js
* Leitura de argumentos via CLI para chavear config.js
* Manter sourcemaps em CSS de terceiros (na task Vendor)
* Múltiplas fontes de CSS (como na task Scripts)
* Otimizador / Minificador de CSS

# AUGUSTO (diferenças)

* as tasks usam modulo chamado log, para output no terminal
* ‘default' com ASCII Art bacana
* ‘watch’ usando chokidar - bem mais eficiente
* ’styles’ (e outros) usando gulp-plumber para tratar os erros
* ‘markup’ (htmls) com funcao para remover dupla linha em branco
* ‘markup’ incorpora funcao para filtros do nunjucks (server-side)
* ‘guideline’ task nova para tratamento do guideline
* ‘atom’ task nova para criar componente para guideline
* ‘deploy’ funcional; verificando existência do rsync