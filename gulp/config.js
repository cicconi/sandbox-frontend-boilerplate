var src = './app';
var dst = './public';
var vendor = './bower_components';

var config = {

  livereload: {
    files: dst + '/**',
    base_dir: dst,
    index: 'index.html'
  },

  styles: {
    sourcemaps: true,
    autoprefixer: ['last 2 versions'],
    paths: [vendor],
    src: src + '/less/main.less',
    dst: dst + '/css'
  },

  images: {
    optimize: false,
    // ignore_src: [src + '/img/ico'],
    src: src + '/img/**/*.{jpg,jpeg,png,gif,svg}',
    dst: dst + '/img'
  },

  sprites: {
    files: {
      // 'ico': src + '/img/ico/*.png',
    },
    optimize: false,
    padding: 10,
    less_dst: src + '/less/generated',
    img_dst: dst + '/img'
  },

  htmls: {
    nunjucks: true,
    output: 'normal', // 'pretty' ou 'minified'
    nunjucks_src: src,
    src: [src + '/**/*.html', '!'+ src + 'nunj/**'],
    dst: dst
  },
  
  scripts: [{
    sourcemaps: true,
    uglify: false,
    // extensions: ['.coffee'],
    src: src + '/js/main.js',
    dst: dst + '/js'
  }],
  
  fonts: {
    src: src + '/fonts/**/*.{ttf,eot,otf,woff,svg}',
    dst: dst + '/fonts'
  },
  
  jsons: {
    src: src + '/json/**',
    dst: dst + '/json'
  },

  vendors: [{
    src: [
      vendor + '/jquery/dist/jquery.js',
      // vendor + '/bootstrap/dist/js/bootstrap.js',
    ],
    dst: dst + '/js/vendor.min.js',
    uglify: true
  }, {
    src: vendor + '/bootstrap/dist/fonts/**',
    dst: dst + '/fonts'
  }],

  deploy: {
    src: dst,
    dst: 'user@digitalbox.cc/path/to/app',
    hint: 'minha sogra é'
  },

  src: src,
  dst: dst
}

/*
config_hom = {
  styles:  { sourcemaps: false },
  scripts: { sourcemaps: false },
  images:  { optimize: true },
  htmls:   { prettify: true },
  deploy: {
    src: './deliverable',
    dst: 'user@digitalbox.cc/path/to/hom',
    hint: 'minha sogra é'
  }
}
*/

module.exports = config;