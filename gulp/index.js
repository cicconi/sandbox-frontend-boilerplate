var fs  = require('fs');
var R   = require('ramda');

var setup = R.pipe(
  R.filter(R.match(/(\.(js|coffee)$)/i)),
  R.map( R.pipe( 
    R.add('./tasks/'), 
    require
  ))
);

setup(fs.readdirSync('./gulp/tasks/'));