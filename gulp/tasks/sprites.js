var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var spritesmith  = require('gulp.spritesmith');
var imagemin     = require('gulp-imagemin');
var config       = require('../config').sprites;

var spriter = function(sprite_name) {

  var stream;
  var system_name = sprite_name;
  var name = sprite_name.replace(/[\/\\]/g, '-');
  var source = config.files[sprite_name];
  var opts = {
    imgName   : system_name + '.png',
    cssName   : name + '.less',
    cssVarMap : function (sprite) {
      sprite.name = name + '-' + sprite.name;
    },
    imgPath   : '../img/' + system_name + '.png',
    cssFormat : 'less',
    algorithm : 'binary-tree',
    engine    : 'pngsmith',
    padding   : config.padding,
  };

  stream = gulp.src(source)
    .pipe(spritesmith(opts));

  stream.css
    .pipe(gulp.dest(config.less_dst));
  
  stream.img
    .pipe(gulpif(config.optimize, imagemin()))
    .pipe(gulp.dest(config.img_dst));
}

var init = module.exports = function() {
  
  Object.keys(config.files).forEach(spriter);
    
}

gulp.task('sprites', init);