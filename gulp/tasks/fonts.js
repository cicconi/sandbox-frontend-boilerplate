var gulp            = require('gulp');
var config          = require('../config').fonts;

var init = module.exports = function() {
  gulp.src(config.src)
     .pipe(gulp.dest(config.dst))
}

gulp.task('fonts', init);