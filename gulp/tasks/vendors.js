var gulp         = require('gulp');
var path         = require('path');
var filter       = require('gulp-filter');
var gulpif       = require('gulp-if');
var uglify       = require('gulp-uglify');
var concat       = require('gulp-concat');
var config       = require('../config');
var packages     = config.vendors;

var packer = function (pack) {

  var is_file     = path.extname(pack.dst).length > 0;
  var pack_dst    = is_file? path.dirname(pack.dst) : pack.dst;
  var filter_js   = filter('**/*.js');
  var filter_text = filter('**/*.{js,css}');
  
  gulp.src(pack.src)
    .pipe(filter_js)
    .pipe(gulpif(pack.uglify, uglify()))
    .pipe(filter_js.restore())
    .pipe(filter_text)
    .pipe(gulpif(is_file, concat(path.basename(pack.dst))))
    .pipe(filter_text.restore())
    .pipe(gulp.dest(pack_dst));
}

var init = module.exports = function() {
  
  packages.forEach(packer);
    
}

gulp.task('vendors', init);