var gulp         = require('gulp');
var gulpif       = require('gulp-if');
var less         = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps   = require('gulp-sourcemaps');
var handle       = require('../util/handleErrors');
var config       = require('../config').styles;

var init = module.exports = function() {
  gulp.src(config.src)
     .pipe(gulpif(config.sourcemaps, sourcemaps.init()))
     .pipe(less({ paths: config.paths }))
     .on('error', handle)
     .pipe(autoprefixer({ browsers: config.autoprefixer }))
     .pipe(gulpif(config.sourcemaps, sourcemaps.write()))
     .pipe(gulp.dest(config.dst))
}

gulp.task('styles', init);