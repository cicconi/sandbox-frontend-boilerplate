// https://github.com/greypants/gulp-starter/blob/master/gulp/tasks/browserify.js
var gulp         = require('gulp');
var path         = require('path');
var browserify   = require('browserify');
var watchify     = require('watchify');
var source       = require('vinyl-source-stream');
var buffer       = require('vinyl-buffer');
var gulpif       = require('gulp-if');
var uglify       = require('gulp-uglify');
var bundleLogger = require('../util/bundleLogger');
var handleErrors = require('../util/handleErrors');
var config       = require('../config').scripts;

gulp.task('scripts', function (callback) {

  var bundleQueue = config.length;

  var browserifyThis = function(bundleConfig) {

    var outputName = path.basename(bundleConfig.src, path.extname(bundleConfig.src)) + '.js';

    var bundler = browserify({
      cache: {}, packageCache: {}, fullPaths: true,
      entries: bundleConfig.src,
      extensions: bundleConfig.extensions,
      debug: bundleConfig.sourcemaps
    });

    var bundle = function() {
      bundleLogger.start(outputName);

      return bundler
        .bundle()
        .on('error', handleErrors)
        .pipe(source(outputName))
        .pipe(buffer())
        .pipe(gulpif(bundleConfig.uglify, uglify()))
        .pipe(gulp.dest(bundleConfig.dst))
        .on('end', reportFinished);
    };

    if(global.isWatching) {
      bundler = watchify(bundler);
      bundler.on('update', bundle);
    }

    var reportFinished = function() {
      bundleLogger.end(outputName)

      if(bundleQueue) {
        bundleQueue--;
        if(bundleQueue === 0) {
          // https://github.com/gulpjs/gulp/blob/master/docs/API.md#accept-a-callback
          callback();
        }
      }
    };

    return bundle();
  };

  config.forEach(browserifyThis);
});