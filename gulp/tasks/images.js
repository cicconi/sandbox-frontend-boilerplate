var gulp         = require('gulp');
var path         = require('path');
var gulpif       = require('gulp-if');
var changed      = require('gulp-changed');
var imagemin     = require('gulp-imagemin');
var config       = require('../config').images;

var init = module.exports = function() {
  
  var ignore = (function (paths) {
    return paths.map(function (src) {
      // https://github.com/gulpjs/gulp/issues/165#issuecomment-32613179
      return '!' +  path.dirname(src) + '/{' + path.basename(src) + ',' + path.basename(src) + '/**}';
    });
  }(config.ignore_src || []));

  gulp.src([config.src].concat(ignore))
     .pipe(changed(config.dst))
     .pipe(gulpif(config.optimize, imagemin()))
     .pipe(gulp.dest(config.dst))
}

gulp.task('images', init);