var gulp          = require('gulp');
var browserSync   = require('browser-sync');
var config        = require('../config').livereload;

var init = module.exports = function() {
  browserSync({
    /* http://www.browsersync.io/docs/options/ */
    files: config.files,
    server: { 
      baseDir: config.base_dir,
      index: config.index
    },
    ghostMode: false,
    open: false,
    logPrefix: 'browsersync',
    logConnections: true,
    // https: true,
    // proxy: 'localhost:8080',
    // tunnel: 'xpto-project',
  });
};

gulp.task('livereload', ['preprocessors'], init);