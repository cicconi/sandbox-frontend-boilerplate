var gulp            = require('gulp');
var config          = require('../config').jsons;

var init = module.exports = function() {
  gulp.src(config.src)
     .pipe(gulp.dest(config.dst))
}

gulp.task('jsons', init);