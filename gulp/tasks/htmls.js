var gulp            = require('gulp');
var gulpif          = require('gulp-if');
var nunjucksRender  = require('gulp-nunjucks-render');
var prettify        = require('gulp-html-prettify');
var minify          = require('gulp-minify-html');
var config          = require('../config').htmls;

if(config.nunjucks) {
  nunjucksRender.nunjucks.configure([config.nunjucks_src]);
}

var init = module.exports = function() {
  gulp.src(config.src)
     .pipe(gulpif(config.nunjucks, nunjucksRender()))
     .pipe(gulpif(config.output === 'pretty', prettify({ indent_char: ' ', indent_size: 4 })))
     .pipe(gulpif(config.output === 'minified', minify()))
     .pipe(gulp.dest(config.dst))
}

gulp.task('htmls', init);