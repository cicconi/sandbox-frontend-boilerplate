var gulp          = require('gulp');
var config        = require('../config');

gulp.task('watch', [
  'set-watch', 
  'livereload'
], function() {
  gulp.watch(config.src + '/less/**', ['styles']);
	gulp.watch(config.src + '/**/*.{html,nunj}', ['htmls']);
});
