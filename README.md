# Frontend Boilerplate

Esqueleto frontend repleto de boas práticas. [download](https://bitbucket.org/digitalboxcc/sandbox-frontend-boilerplate/get/master.zip).

## O que é um _boilerplate_?

Segundo a wikipedia:
> In computer programming, boilerplate code or boilerplate is the sections of code that have to be included in many places with little or no alteration.

Após um tempo de uso, associamos três benefícios diretos ao uso desta estrutura inicial:

1. repetição de código diminuída drasticamente
2. reunião das melhores práticas
3. alinhamento do conhecimento dos envolvidos.

## Quero usar esta estrutura em meu projeto

Entre na página de [downloads do bitbucket](https://bitbucket.org/digitalboxcc/sandbox-frontend-boilerplate/downloads), 
selecione a aba **Branches** e faça download no formato que convém do branch **master**.

[Download de Master (zip)](https://bitbucket.org/digitalboxcc/sandbox-frontend-boilerplate/get/master.zip) - útlima versão estável

## Documentação

* Entenda como [instalar](https://bitbucket.org/digitalboxcc/sandbox-frontend-boilerplate/src/7787507f6e5c0ed46b446ec822b6dbec50fbf8dc/docs/INSTALL.md?at=gulp-starter)
* Consulte o [FAQ](https://bitbucket.org/digitalboxcc/sandbox-frontend-boilerplate/src/7787507f6e5c0ed46b446ec822b6dbec50fbf8dc/docs/FAQ.md?at=gulp-starter)
* Dê [feedback](https://bitbucket.org/digitalboxcc/sandbox-frontend-boilerplate/issues) através de issues.